import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {FilmComponent} from "./components/film/film.component";
import {FilmResolverService} from "./api/film/services/film-resolver.service";
import {FilmDetailsComponent} from "./components/film-details/film-details.component";
import {SolutionsComponent} from "./components/solutions/solutions.component";

const routes: Routes = [
    {
        path: 'films',
        component: FilmComponent,
        resolve: {
            films: FilmResolverService
        }
    },
    {
        path: 'film-details/:id',
        component: FilmDetailsComponent
    },
    {
        path: 'task-solution/:number',
        component: SolutionsComponent
    },
    {
        path: '**',
        redirectTo: 'films'
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
