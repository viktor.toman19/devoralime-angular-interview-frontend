export class SpeciesModel implements ISpeciesModel {
    name!: string;
    classification!: string;
    designation!: string;
    average_height!: string;
    skin_colors!: string;
    hair_colors!: string;
    eye_colors!: string;
    average_lifespan!: string;
    homeworld!: string;
    language!: string;
    people!: string;
    films!: string;
    url!: string

    constructor(data: ISpeciesModel) {
        if (data) {
            for (const property in data) {
                if (data.hasOwnProperty(property)) {
                    (this as any)[property] = (data as any)[property];
                }
            }
        }
    }
}

export interface ISpeciesModel {
    name: string;
    classification: string;
    designation: string;
    average_height: string;
    skin_colors: string;
    hair_colors: string;
    eye_colors: string;
    average_lifespan: string;
    homeworld: string;
    language: string;
    people: string;
    films: string;
    url: string
}
