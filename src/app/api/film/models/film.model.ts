export class FilmModel implements IFilm {
    id!: number;
    title!: string;
    episode_id!: number;
    opening_crawl!: string;
    director!: string;
    producer!: string;
    release_date!: Date;
    characters!: string;
    planets!: string;
    starships!: string;
    vehicles!: string;
    species!: string;
    url!: string;
    cover?: string;

    constructor(data?: IFilm) {
        if (data) {
            for (const property in data) {
                if (data.hasOwnProperty(property)) {
                    (this as any)[property] = (data as any)[property];
                }
            }
        }
    }
}

export interface IFilm {
    id: number;
    title: string;
    episode_id: number;
    opening_crawl: string;
    director: string;
    producer: string;
    release_date: Date;
    characters: string;
    planets: string;
    starships: string;
    vehicles: string;
    species: string;
    url: string;
    cover?: string;
}
