export class FilmsModel implements IFilmsModel {
    title!: string;

    constructor(data?: IFilmsModel) {
        if (data) {
            for (const property in data) {
                if (data.hasOwnProperty(property)) {
                    (this as any)[property] = (data as any)[property];
                }
            }
        }
    }
}

export interface IFilmsModel {
    title: string;
}
