export class CharacterModel implements ICharacterModel {
    name!: string;
    displayName?: string;
    displayFilms: string[] = [];
    height!: string;
    mass!: string;
    hair_color!: string;
    skin_color!: string;
    eye_color!: string;
    birth_year!: string;
    gender!: string;
    homeworld!: string;
    films!: string[];
    species!: string[];
    vehicles!: string[];
    starships!: string[];
    created!: string;
    edited!: string;
    url!: string;
    speciesName?: string;

    constructor(data?: ICharacterModel) {
        if (data) {
            for (const property in data) {
                if (data.hasOwnProperty(property)) {
                    (this as any)[property] = (data as any)[property];
                }
            }
        }
    }
}

export interface ICharacterModel {
    name: string;
    displayName?: string;
    displayFilms?: string[];
    height: string;
    mass: string;
    hair_color: string;
    skin_color: string;
    eye_color: string;
    birth_year: string;
    gender: string;
    homeworld: string;
    films: string[];
    species: string[];
    vehicles: string[];
    starships: string[];
    created: string;
    edited: string;
    url: string;
    speciesName?: string;
}
