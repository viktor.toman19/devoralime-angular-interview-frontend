export class PeopleModel implements IPeopleModel {
    id!: number;
    name!: string;
    height!: string;
    mass!: string;
    hair_color!: string;
    skin_color!: string;
    eye_color!: string;
    birth_year!: string;
    gender!: string;
    homeworld!: string;
    films!: string;
    species!: string;
    vehicles!: string;
    starships!: string;
    url!: string;

    constructor(data?: IPeopleModel) {
        if (data) {
            for (const property in data) {
                if (data.hasOwnProperty(property)) {
                    (this as any)[property] = (data as any)[property];
                }
            }
        }
    }
}

export interface IPeopleModel {
    id: number;
    name: string;
    height: string;
    mass: string;
    hair_color: string;
    skin_color: string;
    eye_color: string;
    birth_year: string;
    gender: string;
    homeworld: string;
    films: string;
    species: string;
    vehicles: string;
    starships: string;
    url: string;
}
