export class VehicleModel implements IVehicleModel {
    id!: number;
    name!: string;
    model!: string;
    manufacturer!: string;
    cost_in_credits!: string;
    length!: string;
    max_atmosphering_speed!: string;
    crew!: string;
    passengers!: string;
    cargo_capacity!: string;
    consumables!: string;
    vehicle_class!: string;
    pilots!: string;
    films!: string;
    url!: string;

    constructor(data?: IVehicleModel) {
        if (data) {
            for (const property in data) {
                if (data.hasOwnProperty(property)) {
                    (this as any)[property] = (data as any)[property];
                }
            }
        }
    }
}

export interface IVehicleModel {
    id: number;
    name: string;
    model: string;
    manufacturer: string;
    cost_in_credits: string;
    length: string;
    max_atmosphering_speed: string;
    crew: string;
    passengers: string;
    cargo_capacity: string;
    consumables: string;
    vehicle_class: string;
    pilots: string;
    films: string;
    url: string;
}
