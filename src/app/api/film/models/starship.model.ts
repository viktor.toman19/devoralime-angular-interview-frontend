export class StarshipModel implements IStarshipModel {
    id!: number;
    name!: string;
    model!: string;
    manufacturer!: string;
    cost_in_credits!: string;
    length!: string;
    max_atmosphering_speed!: string;
    crew!: string;
    passengers!: string;
    cargo_capacity!: string;
    consumables!: string;
    hyperdrive_rating!: string;
    MGLT!: string;
    starship_class!: string;
    pilots!: string;
    films!: string;
    url!: string

    constructor(data?: IStarshipModel) {
        if (data) {
            for (const property in data) {
                if (data.hasOwnProperty(property)) {
                    (this as any)[property] = (data as any)[property];
                }
            }
        }
    }
}

export interface IStarshipModel {
    id: number;
    name: string;
    model: string;
    manufacturer: string;
    cost_in_credits: string;
    length: string;
    max_atmosphering_speed: string;
    crew: string;
    passengers: string;
    cargo_capacity: string;
    consumables: string;
    hyperdrive_rating: string;
    MGLT: string;
    starship_class: string;
    pilots: string;
    films: string;
    url: string
}
