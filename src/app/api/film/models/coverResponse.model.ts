import {CoverModel} from "./cover.model";

export class CoverResponseModel implements ICoverResponseModel {
    page!: number;
    total_pages!: number;
    total_results!: number;
    results!: CoverModel[];

    constructor(data?: ICoverResponseModel) {
        if (data) {
            for (const property in data) {
                if (data.hasOwnProperty(property)) {
                    (this as any)[property] = (data as any)[property];
                }
            }
        }
    }
}

export interface ICoverResponseModel {
    page: number;
    total_pages: number;
    total_results: number;
    results: CoverModel[];
}
