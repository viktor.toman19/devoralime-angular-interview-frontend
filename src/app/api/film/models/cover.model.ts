export class CoverModel implements ICoverModel {
    id!: number;
    title!: string;
    original_title!: string;
    original_language!: string;
    overview!: string;
    adult!: boolean;
    video!: boolean;
    backdrop_path!: string;
    poster_path!: string;
    release_date!: string;
    genre_ids!: number[];
    popularity!: number;
    vote_average!: number;
    vote_count!: number;

    constructor(data?: ICoverModel) {
        if (data) {
            for (const property in data) {
                if (data.hasOwnProperty(property)) {
                    (this as any)[property] = (data as any)[property];
                }
            }
        }
    }
}

export interface ICoverModel {
    id: number;
    title: string;
    original_title: string;
    original_language: string;
    overview: string;
    adult: boolean;
    video: boolean;
    backdrop_path: string;
    poster_path: string;
    release_date: string;
    genre_ids: number[];
    popularity: number;
    vote_average: number;
    vote_count: number;
}
