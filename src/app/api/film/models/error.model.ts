export class ErrorModel implements IError {
    statusCode!: number;
    message!: string;

    constructor(data?: IError) {
        if (data) {
            for (const property in data) {
                if (data.hasOwnProperty(property)) {
                    (this as any)[property] = (data as any)[property];
                }
            }
        }
    }
}

export interface IError {
    statusCode: number;
    message: string;
}
