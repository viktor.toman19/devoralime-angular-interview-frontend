import {StarshipModel} from "./starship.model";
import {VehicleModel} from "./vehicle.model";

export class Solution4Model implements ISolution4Model {
    starships!: StarshipModel[];
    vehicles!: VehicleModel[];

    constructor(data?: ISolution4Model) {
        if (data) {
            for (const property in data) {
                if (data.hasOwnProperty(property)) {
                    (this as any)[property] = (data as any)[property];
                }
            }
        }
    }
}

export interface ISolution4Model {
    starships: StarshipModel[];
    vehicles: VehicleModel[];
}
