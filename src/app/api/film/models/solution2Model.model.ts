import {FilmsModel} from "./films.model";
import {PlanetModel} from "./planet.model";

export class Solution2Model implements ISolution2Model {
    films!: FilmsModel[];
    planets!: PlanetModel[];

    constructor(data?: ISolution2Model) {
        if (data) {
            for (const property in data) {
                if (data.hasOwnProperty(property)) {
                    (this as any)[property] = (data as any)[property];
                }
            }
        }
    }
}

export interface ISolution2Model {
    films: FilmsModel[];
    planets: PlanetModel[];
}
