export class PlanetModel implements IPlanetModel {
    id!: number;
    name!: string;
    rotation_period!: string;
    orbital_period!: string;
    diameter!: string;
    climate!: string;
    gravity!: string;
    terrain!: string;
    surface_water!: string;
    population!: string;
    residents!: string;
    films!: string;
    url!: string;

    constructor(data?: IPlanetModel) {
        if (data) {
            for (const property in data) {
                if (data.hasOwnProperty(property)) {
                    (this as any)[property] = (data as any)[property];
                }
            }
        }
    }
}

export interface IPlanetModel {
    id: number;
    name: string;
    rotation_period: string;
    orbital_period: string;
    diameter: string;
    climate: string;
    gravity: string;
    terrain: string;
    surface_water: string;
    population: string;
    residents: string;
    films: string;
    url: string;
}
