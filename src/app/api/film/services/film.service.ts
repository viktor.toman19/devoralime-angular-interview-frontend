import {Injectable} from '@angular/core';
import {environment} from "../../../../environments/environment";
import {map, Observable} from "rxjs";
import {FilmModel} from "../models/film.model";
import {HttpClient} from "@angular/common/http";
import {catchError} from "rxjs/operators";
import {CoverModel} from "../models/cover.model";
import {CoverResponseModel} from "../models/coverResponse.model";
import {ErrorService} from "./error.service";
import {CharacterModel} from "../models/character.model";
import {SpeciesModel} from "../models/species.model";
import {FilmsModel} from "../models/films.model";

@Injectable({
    providedIn: 'root'
})
export class FilmService {

    apiUrl: string = `${environment.apiUrl}films`;
    externalApiUrl: string = environment.externalApiUrl;

    constructor(
        private http: HttpClient,
        private error: ErrorService
    ) {}

    // Get all film from backend
    getAll(): Observable<FilmModel[]> {
        return this.http.get<FilmModel[]>(`${this.apiUrl}/get-films`).pipe(
            map((res: FilmModel[]) => {
                return res;
            }),
            catchError(this.error.handleError<FilmModel[]>('getAll')),
            map(dtoList => dtoList.map((dto: FilmModel) => new FilmModel(dto)),
        ))
    }

    // Get one film from backend
    getFilm(id: string): Observable<FilmModel> {
        return this.http.get<FilmModel>(`${this.apiUrl}/${id}`).pipe(
            map((res: FilmModel) => {
                return new FilmModel(res);
            }),
            catchError(this.error.handleError<FilmModel>('getFilm'))
        )
    }

    // Get film data from external API (themovies)
    getFilmDataFromExternalApi(title: string): Observable<CoverModel[]> {
        return this.http.get<CoverResponseModel>(`${this.externalApiUrl + '&query=' + title}`).pipe(
            map((res: CoverResponseModel) => {
                return res.results;
            }),
        )
    }

    getCharacterDataFromExternalApi(url: string): Observable<CharacterModel> {
        return this.http.get<CharacterModel>(url).pipe(
            map((res: CharacterModel) => {
                return new CharacterModel(res);
            }),
        )
    }

    getSpeciesDataFromExternalApi(url: string): Observable<SpeciesModel> {
        return this.http.get<SpeciesModel>(url).pipe(
            map((res: SpeciesModel) => {
                return new SpeciesModel(res);
            }),
        )
    }

    getFilmsDataFromExternalApi(url: string): Observable<FilmsModel> {
        return this.http.get<FilmsModel>(url).pipe(
            map((res: FilmsModel) => {
                return new FilmsModel(res);
            }),
        )
    }
}
