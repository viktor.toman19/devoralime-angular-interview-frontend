import {Injectable} from '@angular/core';
import {environment} from "../../../../environments/environment";
import {forkJoin, map, Observable, of, switchMap} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {PeopleModel} from "../models/people.model";
import {catchError} from "rxjs/operators";
import {ErrorService} from "./error.service";
import {PlanetModel} from "../models/planet.model";
import {FilmModel} from "../models/film.model";
import {Solution2Model} from "../models/solution2Model.model";
import {SpeciesModel} from "../models/species.model";
import {Solution4Model} from "../models/solution4Model.model";
import {StarshipModel} from "../models/starship.model";
import {VehicleModel} from "../models/vehicle.model";
import {FilmService} from "./film.service";

@Injectable({
    providedIn: 'root'
})
export class SolutionService {

    apiUrl: string = environment.apiUrl;

    constructor(
        private http: HttpClient,
        private error: ErrorService,
        private filmService: FilmService
    ) {
    }

    getSolution1(): Observable<PeopleModel[]> {
        return this.http.get<PeopleModel[]>(`${this.apiUrl}peoples/get-peoples`).pipe(
            map((res: PeopleModel[]) => {
                return res;
            }),
            catchError(this.error.handleError<PeopleModel[]>('getSolution1')),
            map(
                dtoList => dtoList.map(
                    (dto: PeopleModel) => new PeopleModel(dto)
                )
                    .filter(
                        (data: PeopleModel) => data.gender === 'male' && data.hair_color === 'blond'
                    )
                    .sort(
                        (a: PeopleModel, b: PeopleModel) => {
                            return parseInt(b.height) - parseInt(a.height);
                        }
                    ),
            ))
    }

    getSolution2(): Observable<Solution2Model> {
        return this.http.get<PlanetModel[]>(`${this.apiUrl}planets/get-planets`).pipe(
            map((res: PlanetModel[]) => {
                return res;
            }),
            catchError(this.error.handleError<PlanetModel[]>('getSolution2')),
            map(
                dtoList => dtoList.map(
                    (dto: PlanetModel) => new PlanetModel(dto)
                )
                    .filter(
                        (data: PlanetModel) => data.terrain.includes('mountains')
                    )
            ),
            switchMap(planets => {
                return this.http.get<FilmModel[]>(`${this.apiUrl}films/get-films`).pipe(
                    map(films => {
                            return {
                                planets: planets,
                                films: films
                            }
                        }
                    )
                )
            }),
            catchError(errorForFirstOrSecondCall => {
                console.error('An error occurred: ', errorForFirstOrSecondCall);
                throw new Error('Error: ' + errorForFirstOrSecondCall.message);
            })
        )
    }

    getSolution3(): Observable<SpeciesModel[]> {
        return this.http.get<SpeciesModel[]>(`${this.apiUrl}species/get-species`).pipe(
            map((res: SpeciesModel[]) => {
                return res;
            }),
            catchError(this.error.handleError<SpeciesModel[]>('getSolution3')),
            map(
                dtoList => dtoList.map(
                    (dto: SpeciesModel) => new SpeciesModel(dto)
                )
                    .filter(
                        (data: SpeciesModel) => data.average_lifespan !== 'unknown' && data.average_lifespan !== 'indefinite'
                    )
            ))
    }

    getSolution4(): Observable<Solution4Model> {
        return this.http.get<StarshipModel[]>(`${this.apiUrl}starships/get-starships`).pipe(
            map((res: StarshipModel[]) => {
                return res;
            }),
            catchError(this.error.handleError<StarshipModel[]>('getSolution4')),
            map(
                dtoList => dtoList.map(
                    (dto: StarshipModel) => new StarshipModel(dto)
                )
                    .filter(
                        (data: StarshipModel) => data.manufacturer.includes('Kuat Drive Yards') &&
                            data.max_atmosphering_speed !== 'undefined' &&
                            data.max_atmosphering_speed !== 'n/a'
                    )
                    .sort(
                        (a: StarshipModel, b: StarshipModel) => {
                            return parseInt(b.max_atmosphering_speed) - parseInt(a.max_atmosphering_speed);
                        }
                    )
            ),
            switchMap(starships => {
                return this.http.get<VehicleModel[]>(`${this.apiUrl}vehicles/get-vehicles`).pipe(
                    map(vehicles => {
                            return {
                                starships: starships,
                                vehicles: vehicles
                                    .filter(
                                        (data: VehicleModel) => data.manufacturer.includes('Kuat Drive Yards') &&
                                            data.max_atmosphering_speed !== 'undefined' &&
                                            data.max_atmosphering_speed !== 'n/a'
                                    )
                                    .sort(
                                        (a: VehicleModel, b: VehicleModel) => {
                                            return parseInt(b.max_atmosphering_speed) - parseInt(a.max_atmosphering_speed);
                                        }
                                    ),
                            }
                        }
                    )
                )
            }),
            catchError(errorForFirstOrSecondCall => {
                console.error('An error occurred: ', errorForFirstOrSecondCall);
                throw new Error('Error: ' + errorForFirstOrSecondCall.message);
            })
        )
    }

    getSolution5() {
        const films = this.http.get<FilmModel[]>(`${this.apiUrl}films/get-films`).pipe(
            map((res: FilmModel[]) => {
                return res;
            }),
            map(
                dtoList => dtoList.map(
                    (dto: FilmModel) => new FilmModel(dto)
                )
                    .filter(
                        (data: FilmModel) => data.title !== 'unknown' &&
                            data.opening_crawl !== 'unknown' &&
                            data.director !== 'unknown' &&
                            data.producer !== 'unknown' &&
                            data.characters !== 'unknown' &&
                            data.planets !== 'unknown' &&
                            data.starships !== 'unknown' &&
                            data.vehicles !== 'unknown' &&
                            data.species !== 'unknown' &&
                            data.url !== 'unknown'
                    )
            )
        );

        const people = this.http.get<PeopleModel[]>(`${this.apiUrl}peoples/get-peoples`).pipe(
            map((res: PeopleModel[]) => {
                return res;
            }),
            map(
                dtoList => dtoList.map(
                    (dto: PeopleModel) => new PeopleModel(dto)
                )
                    .filter(
                        (data: PeopleModel) => data.name !== 'unknown' &&
                            data.height !== 'unknown' &&
                            data.mass !== 'unknown' &&
                            data.hair_color !== 'unknown' &&
                            data.skin_color !== 'unknown' &&
                            data.eye_color !== 'unknown' &&
                            data.birth_year !== 'unknown' &&
                            data.gender !== 'unknown' &&
                            data.homeworld !== 'unknown' &&
                            data.films !== 'unknown' &&
                            data.starships !== 'unknown' &&
                            data.vehicles !== 'unknown' &&
                            data.species !== 'unknown' &&
                            data.url !== 'unknown'
                    )
            )
        );

        const planets = this.http.get<PlanetModel[]>(`${this.apiUrl}planets/get-planets`).pipe(
            map((res: PlanetModel[]) => {
                return res;
            }),
            map(
                dtoList => dtoList.map(
                    (dto: PlanetModel) => new PlanetModel(dto)
                )
                    .filter(
                        (data: PlanetModel) => data.name !== 'unknown' &&
                            data.rotation_period !== 'unknown' &&
                            data.orbital_period !== 'unknown' &&
                            data.diameter !== 'unknown' &&
                            data.climate !== 'unknown' &&
                            data.gravity !== 'unknown' &&
                            data.terrain !== 'unknown' &&
                            data.surface_water !== 'unknown' &&
                            data.population !== 'unknown' &&
                            data.residents !== 'unknown' &&
                            data.films !== 'unknown' &&
                            data.url !== 'unknown'
                    )
            )
        );

        const species = this.http.get<SpeciesModel[]>(`${this.apiUrl}species/get-species`).pipe(
            map((res: SpeciesModel[]) => {
                return res;
            }),
            map(
                dtoList => dtoList.map(
                    (dto: SpeciesModel) => new SpeciesModel(dto)
                )
                    .filter(
                        (data: SpeciesModel) => data.name !== 'unknown' &&
                            data.classification !== 'unknown' &&
                            data.designation !== 'unknown' &&
                            data.average_height !== 'unknown' &&
                            data.skin_colors !== 'unknown' &&
                            data.hair_colors !== 'unknown' &&
                            data.eye_colors !== 'unknown' &&
                            data.average_lifespan !== 'unknown' &&
                            data.homeworld !== 'unknown' &&
                            data.language !== 'unknown' &&
                            data.people !== 'unknown' &&
                            data.films !== 'unknown' &&
                            data.url !== 'unknown'
                    )
            )
        );

        const starships = this.http.get<StarshipModel[]>(`${this.apiUrl}starships/get-starships`).pipe(
            map((res: StarshipModel[]) => {
                return res;
            }),
            map(
                dtoList => dtoList.map(
                    (dto: StarshipModel) => new StarshipModel(dto)
                )
                    .filter(
                        (data: StarshipModel) => data.name !== 'unknown' &&
                            data.model !== 'unknown' &&
                            data.manufacturer !== 'unknown' &&
                            data.cost_in_credits !== 'unknown' &&
                            data.length !== 'unknown' &&
                            data.max_atmosphering_speed !== 'unknown' &&
                            data.crew !== 'unknown' &&
                            data.passengers !== 'unknown' &&
                            data.cargo_capacity !== 'unknown' &&
                            data.consumables !== 'unknown' &&
                            data.hyperdrive_rating !== 'unknown' &&
                            data.MGLT !== 'unknown' &&
                            data.starship_class !== 'unknown' &&
                            data.pilots !== 'unknown' &&
                            data.films !== 'unknown' &&
                            data.url !== 'unknown'
                    )
            )
        );

        const vehicles = this.http.get<VehicleModel[]>(`${this.apiUrl}vehicles/get-vehicles`).pipe(
            map((res: VehicleModel[]) => {
                return res;
            }),
            map(
                dtoList => dtoList.map(
                    (dto: VehicleModel) => new VehicleModel(dto)
                )
                    .filter(
                        (data: VehicleModel) => data.name !== 'unknown' &&
                            data.model !== 'unknown' &&
                            data.manufacturer !== 'unknown' &&
                            data.cost_in_credits !== 'unknown' &&
                            data.length !== 'unknown' &&
                            data.max_atmosphering_speed !== 'unknown' &&
                            data.crew !== 'unknown' &&
                            data.passengers !== 'unknown' &&
                            data.cargo_capacity !== 'unknown' &&
                            data.consumables !== 'unknown' &&
                            data.vehicle_class !== 'unknown' &&
                            data.pilots !== 'unknown' &&
                            data.films !== 'unknown' &&
                            data.url !== 'unknown'
                    )
            )
        );

        return forkJoin([films, people, planets, species, starships, vehicles]);
    }

}
