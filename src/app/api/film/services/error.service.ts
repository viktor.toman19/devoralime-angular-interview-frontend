import {Injectable} from '@angular/core';
import {Observable, of} from "rxjs";
import {ErrorModel} from "../models/error.model";

@Injectable({
    providedIn: 'root'
})
export class ErrorService {
    errorMsg: string = '';

    constructor() {
    }

    handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            console.log(error.error.statusCode)
            this.errorMsg = this.findErrorMsg(error.error);
            return of(result as T);
        };
    }

    // Find error message by error code
    findErrorMsg(errorCode: ErrorModel) {
        let msg: string = '';

        switch (errorCode.statusCode) {
            case 404:
                msg = 'Nem található!';
                break;
            default:
                msg = 'Ismeretlen hiba!';
                break;
        }

        return msg;
    }
}

