import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {Observable} from 'rxjs';
import {FilmService} from "./film.service";
import {FilmModel} from "../models/film.model";

@Injectable({
    providedIn: 'root'
})
export class FilmResolverService implements Resolve<any> {

    constructor(private filmService: FilmService) {}

    resolve(route: ActivatedRouteSnapshot): Observable<FilmModel[]> {
        console.log('Called films resolver...');
        return this.filmService.getAll();
    }
}
