import {Component, Input, OnInit} from '@angular/core';
import {FilmModel} from "../../api/film/models/film.model";

@Component({
    selector: 'app-card',
    templateUrl: './card.component.html',
    styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

    @Input()
    data!: FilmModel;

    textLimit: number = 200;

    constructor() {}

    ngOnInit(): void {}
}
