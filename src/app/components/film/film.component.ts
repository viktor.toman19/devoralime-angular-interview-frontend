import {Component, OnInit} from '@angular/core';
import {FilmModel} from "../../api/film/models/film.model";
import {FilmService} from "../../api/film/services/film.service";
import {ActivatedRoute} from "@angular/router";

@Component({
    selector: 'app-film',
    templateUrl: './film.component.html',
    styleUrls: ['./film.component.scss']
})
export class FilmComponent implements OnInit {

    films: FilmModel[] = [];

    constructor(
        private readonly filmService: FilmService,
        private readonly activatedRoute: ActivatedRoute
    ) {}

    ngOnInit(): void {
        this.activatedRoute.data.subscribe((response: any) => {
            this.films = response.films;

            this.films.map((film: any) => {
                this.filmService.getFilmDataFromExternalApi(film.title).subscribe((response: any) => {
                    let poster = './assets/No_Cover.jpg';

                    if (response.length > 0) {
                        poster = 'https://image.tmdb.org/t/p/w500' + response[0].poster_path;
                    }

                    film.cover = poster;
                })
            })
        });
    }

}
