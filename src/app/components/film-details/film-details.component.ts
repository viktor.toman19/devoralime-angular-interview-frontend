import {Component, OnInit} from '@angular/core';
import {FilmService} from "../../api/film/services/film.service";
import {ActivatedRoute, Router} from "@angular/router";
import {FilmModel} from "../../api/film/models/film.model";
import {CharacterModel} from "../../api/film/models/character.model";
import {finalize} from "rxjs";
import {SpeciesModel} from "../../api/film/models/species.model";
import {FilmsModel} from "../../api/film/models/films.model";

@Component({
    selector: 'app-film-details',
    templateUrl: './film-details.component.html',
    styleUrls: ['./film-details.component.scss']
})
export class FilmDetailsComponent implements OnInit {

    loading: boolean = true;
    id: string | null = '';
    film: FilmModel = new FilmModel();
    characters: string[] = [];
    characterModels: CharacterModel[] = [];

    constructor(
        private readonly filmService: FilmService,
        private readonly route: ActivatedRoute,
        private readonly router: Router
    ) {
    }

    ngOnInit(): void {
        this.id = this.route.snapshot.paramMap.get('id');

        if (this.id) {
            this.filmService.getFilm(this.id).subscribe((filmModel: FilmModel) => {
                this.film = filmModel;
                this.characters = this.film.characters.split(",");

                if (this.characters.length > 0) {
                    this.characters.map((url: string) => {
                        this.filmService.getCharacterDataFromExternalApi(url)
                            .subscribe((characterModel: CharacterModel) => {

                                if (characterModel.species.length > 0) {
                                    characterModel.species.map((species: string) => {
                                        this.filmService.getSpeciesDataFromExternalApi(species)
                                            .subscribe((speciesModel: SpeciesModel) => {
                                                characterModel.speciesName = speciesModel.name;

                                                if (speciesModel.name !== 'Droid') {
                                                    characterModel.displayName = characterModel.name + ' (Species: ' + characterModel.speciesName + ')';
                                                } else {
                                                    characterModel.displayName = characterModel.name;
                                                }
                                            })
                                    })
                                } else {
                                    characterModel.displayName = characterModel.name;
                                }

                                if (characterModel.films.length > 0) {
                                    characterModel.films.map((film: string) => {
                                        this.filmService.getFilmsDataFromExternalApi(film)
                                            .pipe(finalize(() => this.loading = false))
                                            .subscribe((filmsModel: FilmsModel) => {
                                                characterModel.displayFilms.push(filmsModel.title);
                                            })
                                    })
                                } else {
                                    characterModel.displayFilms = [];
                                }

                                this.characterModels.push(characterModel);
                            })
                    })
                }
            })
        } else {
            this.router.navigate(['/films'])
        }
    }

}
