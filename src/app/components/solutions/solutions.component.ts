import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {SolutionService} from "../../api/film/services/solution.service";
import {PeopleModel} from "../../api/film/models/people.model";
import {PlanetModel} from "../../api/film/models/planet.model";
import {Solution2Model} from "../../api/film/models/solution2Model.model";
import {SpeciesModel} from "../../api/film/models/species.model";
import {Solution4Model} from "../../api/film/models/solution4Model.model";
import {FilmModel} from "../../api/film/models/film.model";
import {VehicleModel} from "../../api/film/models/vehicle.model";
import {StarshipModel} from "../../api/film/models/starship.model";

@Component({
    selector: 'app-solutions',
    templateUrl: './solutions.component.html',
    styleUrls: ['./solutions.component.scss']
})
export class SolutionsComponent implements OnInit {

    loading: boolean = true;
    title: string = '';
    solution: string | null = '';
    solutionResponse: string | null = '';

    constructor(
        private route: ActivatedRoute,
        private solutionService: SolutionService
    ) {
    }

    ngOnInit(): void {
        this.solution = this.route.snapshot.paramMap.get('number');

        switch (this.solution) {
            case '1':
                this.getSolution1();
                break;
            case '2':
                this.getSolution2();
                break;
            case '3':
                this.getSolution3();
                break;
            case '4':
                this.getSolution4();
                break;
            case '5':
                this.getSolution5();
                break;
            default:
                this.title = 'No result!';
                this.loading = false;
        }
    }

    getSolution1() {
        this.solutionService.getSolution1().subscribe((people: PeopleModel[]) => {
            let solution = people.length > 0 ? people.shift() : {name: 'No result'};

            if (solution !== undefined) {
                this.solutionResponse = solution.name;
            }

            this.title = 'The tallest blond human';
            this.loading = false;
        })
    }

    getSolution2() {
        this.solutionService.getSolution2().subscribe((result: Solution2Model) => {
            let filmsCount = result.films.length;
            let planetNames: string[] = [];

            let planets = result.planets
                .map(
                    (dto: PlanetModel) => {
                        return new PlanetModel(dto)
                    }
                )
                .filter(
                    (planet: PlanetModel) => planet.films.split(",").length === filmsCount
                )

            planets.map((planet: PlanetModel) => planetNames.push(planet.name))

            this.solutionResponse = planetNames.length > 0 ? planetNames.join(', ') : 'No result';
            this.title = 'All planet which shows up in all film and has mountains';
            this.loading = false;
        })
    }

    getSolution3() {
        this.solutionService.getSolution3().subscribe((species: SpeciesModel[]) => {
            let speciesArr = this.findOcc(species, 'classification')
            let sorted = speciesArr.sort(function (a, b) {
                return b.occurrence - a.occurrence;
            });

            let solution = sorted.length > 0 ? sorted.shift() : {name: 'No result'};

            if (solution !== undefined) {
                this.solutionResponse = solution.name;
            }

            this.loading = false;
            this.title = 'Most common species which has a known average lifespan';
        })
    }

    getSolution4() {
        this.solutionService.getSolution4().subscribe((result: Solution4Model) => {
            let starship = result.starships.length > 0 ? result.starships.shift() : {name: 'No result'};
            let vehicle = result.vehicles.length > 0 ? result.vehicles.shift() : {name: 'No result'};

            if (
                starship !== undefined &&
                vehicle !== undefined
            ) {
                this.solutionResponse = 'Starships: ' + starship.name + ' - Vehicle: ' + vehicle.name
            }

            this.loading = false;
            this.title = 'Fastest starship and vehicle which is manufactured by \'Kuat Drive Yards\'';
        })
    }

    getSolution5() {
        this.solutionService.getSolution5().subscribe((result: any[]) => {
            let films = result[0];
            let people = result[1];
            let planets = result[2];
            let species = result[3];
            let starships = result[4];
            let vehicles = result[5];

            let filmNames: string[] = [];
            let peopleNames: string[] = [];
            let planetNames: string[] = [];
            let speciesNames: string[] = [];
            let starshipNames: string[] = [];
            let vehicleNames: string[] = [];

            films.map((film: FilmModel) => {
                filmNames.push(film.title)
            })

            people.map((person: PeopleModel) => {
                peopleNames.push(person.name)
            })

            planets.map((planet: PlanetModel) => {
                planetNames.push(planet.name)
            })

            species.map((species: SpeciesModel) => {
                speciesNames.push(species.name)
            })

            starships.map((starship: StarshipModel) => {
                starshipNames.push(starship.name)
            })

            vehicles.map((vehicle: VehicleModel) => {
                vehicleNames.push(vehicle.name)
            })

            this.title = 'Every entity which doesn\'t have any unknown property'

            this.solutionResponse = 'Films: ' + filmNames.join(', ') + ' | <br><br>'
            this.solutionResponse += 'Peoples: ' + peopleNames.join(', ') + ' | <br><br>'
            this.solutionResponse += 'Planets: ' + planetNames.join(', ') + ' | <br><br>'
            this.solutionResponse += 'Species: ' + speciesNames.join(', ') + ' | <br><br>'
            this.solutionResponse += 'Starships: ' + starshipNames.join(', ') + ' | <br><br>'
            this.solutionResponse += 'Vehicles: ' + vehicleNames.join(', ')

            this.loading = false;
        })
    }

    private findOcc(arr: any[], key: any) {
        let arr2: any[] = [];

        arr.forEach((x) => {
            if (arr2.some((val) => {
                return val[key] == x[key]
            })) {
                arr2.forEach((k) => {
                    if (k[key] === x[key]) {
                        k["occurrence"]++
                    }
                })

            } else {
                let a: any = {}

                a[key] = x[key]
                a["occurrence"] = 1
                a["name"] = x.name

                arr2.push(a);
            }
        })

        return arr2
    }
}
